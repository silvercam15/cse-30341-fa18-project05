#!/bin/bash

run-test() {
    name=$1
    policy=$2
    EXP1="One Thread: ./func_collatz 8 8 $policy 1"
    EXP2="Three Threads: ./func_collatz 8 8 $policy 3"
    EXP3="Page size 64: ./func_collatz 8 8 $policy 1"
    EXP4="Page size 8: ./func_collatz 8 8 $policy 1"
    EXP5="Address length 64: ./func_collatz 8 8 $policy 1"
    EXP6="Address length 8: ./func_collatz 8 8 $policy 1"
    echo -n "Testing   collatz with $name ... "
    hi="test 1"
    echo ${EXP1} > exp_threads.txt
    (seq 1 100 | ./func_collatz 8 8 $policy 1) >> exp_threads.txt
    echo ${hi}
    echo ${EXP2} > exp_threads.txt
    (seq 1 100 | ./func_collatz 8 8 $policy 3) >> exp_threads.txt
    echo ${EXP3} > exp_threads.txt
    (seq 1 100 | ./func_collatz 64 8 $policy 1) >> exp_threads.txt
    echo {$EXP4} > exp_threads.txt
    (seq 1 100 | ./func_collatz 8 8 $policy 1) >> exp_threads.txt
    echo ${EXP5} > exp_threads.txt
    (seq 1 100 | ./func_collatz 8 8 $policy 1) >> exp_threads.txt
    echo ${EXP6} > exp_threads.txt
    (seq 1 100 | ./func_collatz 8 64 $policy 1) >> exp_threads.txt
    
}

#run-test FIFO   0 
run-test Random 1 
run-test LRU    2 
run-test Clock  3 
