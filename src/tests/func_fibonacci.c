/* func_fibonacci.c: compute fibonacci */

#include "memhashed/cache.h"
#include "memhashed/thread.h"

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

/* Globals */

Cache *Fibonacci = NULL;

/* Thread */

void *	fibonacci_thread(void *arg) {
    size_t tid = (size_t)(arg);

    for (int64_t key = 0; key < 100; key++) {
	printf("%2lu. Fibonacci(%2lu) = %lu\n", tid, key, cache_get(Fibonacci, key));
    }

    return NULL;
}

/* Handler */

int64_t	fibonacci_handler(const uint64_t key) {
    if (key == 0) {
    	return 0;
    }
    if (key <= 2) {
    	return 1;
    }

    return cache_get(Fibonacci, key - 1) + cache_get(Fibonacci, key - 2);
}

/* Main execution */

int main(int argc, char *argv[]) {
    if (argc != 5) {
    	fprintf(stderr, "Usage: %s AddressLength PageSize EvictionPolicy Threads\n", argv[0]);
    	return EXIT_FAILURE;
    }

    size_t addrlen   = strtol(argv[1], NULL, 10);
    size_t page_size = strtol(argv[2], NULL, 10);
    Policy policy    = strtol(argv[3], NULL, 10);
    size_t nthreads  = strtol(argv[4], NULL, 10);

    Fibonacci = cache_create(addrlen, page_size, policy, fibonacci_handler);
    assert(Fibonacci);

    Thread threads[nthreads];
    for (size_t t = 0; t < nthreads; t++) {
    	thread_create(&threads[t], NULL, fibonacci_thread, (void *)t);
    }

    for (size_t t = 0; t < nthreads; t++) {
    	thread_join(threads[t], NULL);
    }

    cache_stats(Fibonacci, stdout);
    cache_delete(Fibonacci);
    return EXIT_SUCCESS;
}
