/* func_collatz.c: compute collatz */

#include "memhashed/cache.h"
#include "memhashed/queue.h"
#include "memhashed/thread.h"

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

/* Constants */

#define SENTINEL (-1)

/* Globals */

Cache  *Collatz = NULL;
Queue  *Numbers = NULL;

/* Threads */

/**
 * Continuously computes the collatz length for the data in the Numbers Queue
 * until the SENTINEL is encountered.
 */
void *	collatz_thread(void *arg) {
    int key = queue_pop(Numbers);
    while(key != SENTINEL){
        printf("Collatz(%d) = %ld\n", key, cache_get(Collatz, key));
        key = queue_pop(Numbers); 
    }
    return NULL;
}

/* Handler */

/**
 * Recursively computes collatz length for specified key.
 * @param   key	    Number to computer length for.
 * @return  Length of collatz sequence for specified key.
 */
int64_t	collatz_handler(const uint64_t key) {
    // base case
    if (key == 1){
        return 1;
    }
    // if odd, add 1
    if (key % 2 == 1){
        return 1 + cache_get(Collatz, (3 * key + 1));
    }
   return 1 + cache_get(Collatz, (key/2));
}

/* Main execution */

int main(int argc, char *argv[]) {
    if (argc != 5) {
    	fprintf(stderr, "Usage: %s AddressLength PageSize EvictionPolicy Threads\n", argv[0]);
    	return EXIT_FAILURE;
    }

    // TODO: Parse command line arguments
    int address_length = atoi(argv[1]); /* number of bits in address space */
    int page_size      = atoi(argv[2]);      /* number of entries per page */
    Policy policy      = atoi(argv[3]);         /* which eviction policty to use */
    int num_threads    = atoi(argv[4]);

    // TODO: Create Collatz Cache
    Collatz = cache_create(address_length, 
                           page_size, 
                           policy,
                           collatz_handler);

    // TODO: Create Numbers Queue
    Numbers = queue_create(SENTINEL, BUFSIZ); /* not sure what capacity should be */

    // TODO: Create Worker threads
    Thread worker[num_threads];
    for(int i = 0; i < num_threads; i++){
        thread_create(&worker[i], 0, collatz_thread, Numbers);
    }

    // TODO: Read numbers from standard input and add to Numbers queue
    int current_num = 0;
    while(fscanf(stdin, "%d", &current_num) != EOF){
        queue_push(Numbers, current_num);
    }
    queue_push(Numbers, SENTINEL);

    // TODO: Join Worker threads
    for(int i = 0; i < num_threads; i++){
        thread_join(worker[i], NULL);
    }

    // TODO: Output Collatz Cache statistics
    cache_stats(Collatz, stdout); 
    
    // TODO: Delete Collatz Cache and Numbers Queue
    cache_delete(Collatz);
    queue_delete(Numbers);
    return EXIT_SUCCESS;
}
