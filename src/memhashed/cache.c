/* cache.c: MemHashed Cache structure */

#include "memhashed/cache.h"
#include <stdlib.h>
#include <math.h>

/* Cache Functions */

/**
 * Create Cache Structure.
 * @param   addrlen     Length of virtual address.
 * @param   page_size   Number of entries per page.
 * @param   policy      Which eviction policy to use.
 * @param   handler     Handler function to call on cache misses.
 * @return  Newly allocated Cache Structure with allocated pages.
 */
Cache *	cache_create(size_t addrlen, size_t page_size, Policy policy, Handler handler) {
    // TODO: Allocate Cache Structure and configure internal fields.
    Cache* c = (Cache*)calloc(1, sizeof(Cache));
    if(!c){
        return NULL; 
    }
    
    c->addrlen = addrlen;
    c->page_size = page_size;
    c->policy = policy;
    c->handler = handler;

    if(addrlen || page_size){
        c->naddresses = pow(2, addrlen);
        c->npages = c->naddresses / page_size;
        c->vpn_shift = addrlen - log2(c->npages);

        /* Calculate vpn_mask */
        int len = addrlen - (c->vpn_shift);
        int power_of_2 = --addrlen;
        for(int i = 0; i < len; i++){
            c->vpn_mask += pow(2, (power_of_2--));
        }

        /* Calculate offset_mask */
        len = c->vpn_shift;
        power_of_2 = 0;
        for(int i = 0; i < len; i++){
            c->offset_mask += pow(2, (power_of_2++));
        }
    
        /* Allocate pages */
        c->pages = (Page**)calloc(c->npages, sizeof(Page*));
        for(int i = 0; i < c->npages; i++){
            c->pages[i] = page_create(page_size, policy);
        }
        mutex_init(&c->lock, NULL);
    }
    return c;
}

/**
 * Delete Cache Structure (including internal pages).
 * @param   cache       Pointer to Cache Structure.
 */
void    cache_delete(Cache *cache) {
    // TODO: Deallocate Cache Structure and internal fields.
    if(cache){
        if(cache->pages){
            for(int i = 0; i < cache->npages; i++){
                page_delete(cache->pages[i]);
            }
            free(cache->pages);
        }
        free(cache);
    } 
}

/**
 * Retrieves value for specified for key:
 
 *  1. Generate the virtual address.
 *
 *  2. Compute VPN and offset of virtual ddress using cache masks and shifts.
 *
 *  3. Use VPN and offset to lookup appropriate page:
 *
 *      a. If entry is found, then return value.
 *
 *      b. Otherwise, use the handler to generate the value for the
 *      corresponding key.
 *
 * @param   cache       Pointer to Cache Structure.
 * @param   key         Key used to identify corresponding Entry.
 * @return  value for the correspending key.
 */
int64_t	cache_get(Cache *cache, const uint64_t key) {
    mutex_lock(&cache->lock);
    int64_t value;

    // # 1:
    int64_t VA = key % cache->naddresses;

    // # 2: 
    int64_t VPN = VA >> cache->vpn_shift; // cache->vpn_mask >> cache->vpn_shift;
    int64_t offset = VA & cache->offset_mask;

    // # 3:
    Page** pages = cache->pages;
    // a.
    if(pages[VPN]->entries[offset].key == key) { 
        cache->hits++;
        value = pages[VPN]->entries[offset].value;
        mutex_unlock(&cache->lock);
        return value;
    }
    // b.
    else {
        cache->misses++;
        mutex_unlock(&cache->lock);
        value = cache->handler(key);
        page_put(pages[VPN], key, value, offset);
        return(value);
    }
    return 0;
}


/**
 * Insert or update value for specified key:
 *
 *  1. Generate the virtual address.
 *
 *  2. Compute VPN and offset of virtual ddress using cache masks and shifts.
 *
 *  3. Use VPN and offset to insert or update value into appropriate page.
 *
 * @param   cache       Pointer to Cache Structure.
 * @param   key         Key used to identify corresponding Entry.
 * @param   value       Value to update or replace old value with.
 */
void	cache_put(Cache *cache, const uint64_t key, const int64_t value) {
    // # 1:
    int64_t VA = key % cache->naddresses;

    // # 2:
    int64_t VPN = VA >> cache->vpn_shift;
    int64_t offset = VA & cache->offset_mask;
   
    
    // # 3:
    Page** pages = cache->pages;
    if(pages[VPN]->entries[offset].key != key) {
        pages[VPN]->entries[offset].key = key;
        pages[VPN]->entries[offset].value = value;
    }
    else{
        pages[VPN]->entries[offset].value = value;
    }
}

/**
 * Display cache hits and misses to specified streams.
 * @param   cache       Pointer to Cache Structure.
 * @param   stream      File stream to write to.
 */
void	cache_stats(Cache *cache, FILE *stream) {
    mutex_lock(&cache->lock);
    fprintf(stream, "Hits   = %lu\n", cache->hits);
    fprintf(stream, "Misses = %lu\n", cache->misses);
    fflush(stdout);
    mutex_unlock(&cache->lock);
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
