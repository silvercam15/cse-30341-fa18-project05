/* page.c: MemHashed Page structure */
#include "memhashed/page.h"

/* Prototypes */

size_t  page_evict_fifo(Page *page, size_t offset);
size_t  page_evict_random(Page *page, size_t offset);
size_t  page_evict_lru(Page *page, size_t offset);
size_t  page_evict_clock(Page *page, size_t offset);

/* Functions */

/**
 * Create Page Structure.
 * @param   nentries    Number of entries in page.
 * @param   policy      Which eviction policy to use.
 * @return  Newly allocated Page Structure with allocated entries.
 */
Page *	page_create(size_t nentries, Policy policy) {
    // TODO: Allocate Page Structure and internal fields.
    Page* p = (Page*)calloc(1, sizeof(Page));
    if(!p){
        return NULL;
    }


    p->policy = policy;
    p->nentries = nentries;
    
    if(nentries){
        p->entries = (Entry*)calloc(nentries, sizeof(Entry));
        for(int i = 0; i < nentries; i++){
            p->entries[i].key = -1;
        }
        mutex_init(&p->lock, NULL);
    } 
        
    return p;
}

/**
 * Delete Page Structure (including internal entries).
 * @param   page        Pointer to Page Structure.
 */
void    page_delete(Page *page) {
    // TODO: Deallocate Page Structure and internal fields.
    if(page){
        if (page->entries){
            free(page->entries);
        }
        free(page);
    }
}

/**
 * Searches Page for Entry with specified key, beginning at specified offset.
 *
 *  This function performs a linear probe of the entries array beginning with
 *  the specified offset until a matching key is found or all entries have been
 *  probed.
 *
 * @param   page        Pointer to Page Structure.
 * @param   key         Key used to identify corresponding Entry.
 * @param   offset      Initial offset to begin probe.
 * @return  Entry with corresponding key or an Entry with a mismatched key if not found.
 */
Entry   page_get(Page* page, const uint64_t key, size_t offset){
    /* Error checkout for the value of offset */
    if(offset >= page->nentries){
        Entry entry = {0};
        entry.key = ~key;
        return entry;
    }

    Entry entry = {0};
    Entry* entries = page->entries;
    bool found = false;
    int i;
    for(i = offset; i < page->nentries; i++){
        if(entries[i].key == key && entries[i].valid){
            (page->used)++;
            entries[i].used = page->used; 
            
            entry = entries[i];
            found = true;
            break;
        }
    }

    if(!found){ 
        for(i = 0; i < offset; i++){
            if(entries[i].key == key && entries[i].valid){
                (page->used)++;
                entries[i].used = page->used; 
                
                entry = entries[i];
                found = true;
            }
        }

        if(!found){
            entry.key = ~key;
        }
    }

    return entry;
}

/**
 * Insert or update Entry with specified key with the new value.
 *
 *  This function begins a linear probe for the Entry with the corresponding
 *  key:
 *
 *      1. If it is found, then the Entry is updated.
 *
 *      2. If it is not found, then the first invalid Entry is used to insert a
 *      new key and value.
 *
 *      3. If it is not found and there are no invalid Entries, then one is
 *      evicted based on the previously specified Policy and then used
 *      to insert a new key and value.
 *
 * @param   page        Pointer to Page Structure.
 * @param   key         Key used to identify corresponding Entry.
 * @parm    value       Value to update or replace old value with.
 * @param   offset      Initial offset to begin probe.
 */
void    page_put(Page *page, const uint64_t key, const int64_t value, size_t offset) {
    Entry* entries = page->entries;
    bool item_found = false;
    for(int i = offset; i < page->nentries; i++){
        /* found */
        if(entries[i].key == key){ 
            entries[i].value = value;
            (page->used)++;
            entries[i].used = page->used;
            entries[i].valid = true;
            item_found = true;
            return;
        }
    }
   
    /* not found - look for next invalid Entry */
    bool invalid_found = false;
    if(!item_found){ 
        for(int i = offset; i < page->nentries; i++){
            if(!entries[i].valid){
                entries[i].key = key;
                entries[i].value = value;
                entries[i].valid = true;
                (page->used)++;
                entries[i].used = page->used;
                page->order++;
                entries[i].order = page->order;
                invalid_found = true;  
                return;
            }
        }
    }
    
    /* No invalid entries found, evict using specified policy */
    if(!item_found && !invalid_found){
        size_t evict = 0;
        if(page->policy == FIFO){
            evict = page_evict_fifo(page, offset);
        } else if(page->policy == RANDOM){
            evict = page_evict_random(page, offset);
        } else if(page->policy == LRU){
            evict = page_evict_lru(page, offset);
        } else if(page->policy == CLOCK){
            evict = page_evict_clock(page, offset);
        } 
        
        entries[evict].value = value;
        entries[evict].key = key;
       
        (page->used)++;
        entries[evict].used = page->used;
 
        page->order++;
        entries[evict].order = page->order;
        item_found = true;
    }
}

/**
 * Select Entry to evict based on FIFO strategy.
 * @param   page        Pointer to Page Structure.
 * @param   offset      Initial offset to begin probe.
 * @return  Offset of Entry to evict.
 */
size_t  page_evict_fifo(Page *page, size_t offset) {
    Entry* entries = page->entries;
    size_t entry_offset = 0;
    size_t order = page->order;
    for(int i = offset; i < page->nentries; i++){
        if(entries[i].order < order){
            entry_offset = i;
            order = entries[i].order;
        }
    }
    return entry_offset;
}

/**
 * Select Entry to evict based on Random strategy.
 * @param   page        Pointer to Page Structure.
 * @param   offset      Initial offset to begin probe.
 * @return  Offset of Entry to evict.
 */
size_t  page_evict_random(Page *page, size_t offset) {
    return (rand() % page->nentries);
}

/**
 * Select Entry to evict based on LRU strategy.
 * @param   page        Pointer to Page Structure.
 * @param   offset      Initial offset to begin probe.
 * @return  Offset of Entry to evict.
 */
size_t  page_evict_lru(Page *page, size_t offset) {
    size_t entry_offset = offset;
    Entry* entries = page->entries;
    size_t used = entries[offset].used;
    for(int i = 0; i < page->nentries; i++){
        if(entries[i].used < used){
            entry_offset = i;
            used = entries[i].used;
        }
    }
    return entry_offset;
}

/**
 * Select Entry to evict based on Clock strategy.
 * @param   page        Pointer to Page Structure.
 * @param   offset      Initial offset to begin probe.
 * @return  Offset of Entry to evict.
 */
size_t  page_evict_clock(Page *page, size_t offset) {
     
    while(true){
        if (page->entries[offset].used)
            page->entries[offset].used = 0;
        else
            return offset;
        offset = (offset+1) % page->nentries;
    }
    
    return offset;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
